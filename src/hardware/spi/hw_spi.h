/*
 * hw_spi.h
 *
 *  Created on: Dec 22, 2018
 *      Author: zhan
 */

#ifndef HARDWARE_HW_SPI_H_
#define HARDWARE_HW_SPI_H_

#include "stm32f4xx.h"
#include "spi_if.h"
#include "hardware/gpio_pin.h"

class HardwareSpi : public SpiInterface{
protected:
	SPI_TypeDef *spix;
	GpioPin *cs;
	GpioPin *clk;
	GpioPin *miso;
	GpioPin *mosi;

	void init();

public:
	HardwareSpi(SPI_TypeDef *spix, GpioPin *cs, GpioPin *clk, GpioPin *miso, GpioPin *mosi) : SpiInterface(){
		this->spix = spix;
		this->cs = cs;
		this->clk = clk;
		this->miso = miso;
		this->mosi = mosi;
		init();
	}

	~HardwareSpi(){

	}

	uint8_t read_write(uint8_t data){
		while (SPI_I2S_GetFlagStatus(spix, SPI_I2S_FLAG_TXE) == RESET){}//等待发送区空
		SPI_I2S_SendData(spix, data); //通过外设SPIx发送一个byte  数据
		while (SPI_I2S_GetFlagStatus(spix, SPI_I2S_FLAG_RXNE) == RESET){} //等待接收完一个byte
		return SPI_I2S_ReceiveData(spix); //返回通过SPIx最近接收的数据
	}

	void enable_chip(){
		cs->write(Bit_RESET);
	}

	void disable_chip(){
		cs->write(Bit_SET);
	}

};


#endif /* HARDWARE_HW_SPI_H_ */
