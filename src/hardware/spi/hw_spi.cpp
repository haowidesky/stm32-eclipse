/*
 * hw_spi.cpp
 *
 *  Created on: Dec 22, 2018
 *      Author: zhan
 */


#include "hw_spi.h"

typedef struct {
	SPI_TypeDef *spix;
	uint8_t gpio_af;
} spi_info_t;

static spi_info_t spi_infos[] = {
		{SPI1, GPIO_AF_SPI1,},
		{SPI2, GPIO_AF_SPI2,},
		{SPI3, GPIO_AF_SPI3,},
		{SPI4, GPIO_AF_SPI4,},
		{SPI5, GPIO_AF_SPI5,},
		{SPI6, GPIO_AF_SPI6,},
};

static void spi_reset_cmd(SPI_TypeDef *spix, FunctionalState state)
{
	if ( spix == SPI1 ){
		RCC_APB2PeriphResetCmd(RCC_APB2Periph_SPI1, state);
	}else if ( spix == SPI2 ){
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_SPI2, state);
	}else if ( spix == SPI3 ){
		RCC_APB1PeriphResetCmd(RCC_APB1Periph_SPI3, state);
	}else if ( spix == SPI4 ){
		RCC_APB2PeriphResetCmd(RCC_APB2Periph_SPI4, state);
	}else if ( spix == SPI5 ){
		RCC_APB2PeriphResetCmd(RCC_APB2Periph_SPI5, state);
	}else if ( spix == SPI6 ){
		RCC_APB2PeriphResetCmd(RCC_APB2Periph_SPI6, state);
	}
}

static spi_info_t *get_spi_info(SPI_TypeDef *spix)
{
	for(size_t i=0;i<sizeof(spi_infos)/sizeof(spi_info_t);i++){
		if ( spix == spi_infos[i].spix ){
			return &spi_infos[i];
		}
	}
	return NULL;
}

void HardwareSpi::init()
{
	cs->set_output(Bit_SET);

	clk->init(GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP);
	miso->init(GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_OD, GPIO_PuPd_UP);
	mosi->init(GPIO_Mode_AF, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP);
	spi_info_t *info = get_spi_info(spix);
	GPIO_PinAFConfig(clk->gpiox, GpioPin::get_pin_source(clk->pin), info->gpio_af);
	GPIO_PinAFConfig(miso->gpiox, GpioPin::get_pin_source(miso->pin), info->gpio_af);
	GPIO_PinAFConfig(mosi->gpiox, GpioPin::get_pin_source(mosi->pin), info->gpio_af);

	spi_reset_cmd(spix, ENABLE);
	spi_reset_cmd(spix, DISABLE);

	SPI_InitTypeDef SPI_InitStructure;
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;  //设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;		//设置SPI工作模式:设置为主SPI
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;	//设置SPI的数据大小:SPI发送接收8位帧结构
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;			//串行同步时钟的空闲状态为高电平
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;		//串行同步时钟的第二个跳变沿（上升或下降）数据被采样
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;			//NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;		//定义波特率预分频的值:波特率预分频值为256
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;	//指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始
	SPI_InitStructure.SPI_CRCPolynomial = 7;			//CRC值计算的多项式
	SPI_Init(spix, &SPI_InitStructure);  				//根据SPI_InitStruct中指定的参数初始化外设SPIx寄存器

	SPI_Cmd(spix, ENABLE); 								//使能SPI外设

	read_write(0xff);									//启动传输
}

