/*
 * spi.h
 *
 *  Created on: Dec 22, 2018
 *      Author: zhan
 */

#ifndef HARDWARE_SPI_IF_H_
#define HARDWARE_SPI_IF_H_

#include "stm32f4xx.h"

class SpiInterface{
protected:
	virtual void init() = 0;

public:
	virtual void enable_chip() = 0;
	virtual void disable_chip() = 0;
	virtual uint8_t read_write(uint8_t data)=0;
};



#endif /* HARDWARE_SPI_IF_H_ */
