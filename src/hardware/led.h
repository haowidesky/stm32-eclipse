/*
 * led.h
 *
 *  Created on: Dec 22, 2018
 *      Author: zhan
 */

#ifndef HARDWARE_LED_H_
#define HARDWARE_LED_H_

#include "stm32f4xx.h"

#include "gpio_pin.h"

class Led{
private:
	GpioPin *pin;
	BitAction on_bit;
	BitAction off_bit;
	bool is_on;
public:
	Led(GpioPin *pin, BitAction on_bit);

	void set_on();
	void set_off();
	void turn();
};




#endif /* HARDWARE_LED_H_ */
