/*
 * led.cpp
 *
 *  Created on: Dec 22, 2018
 *      Author: zhan
 */


#include "led.h"


Led::Led(GpioPin *pin, BitAction on_bit)
{
	this->pin = pin;
	this->on_bit = on_bit;
	this->off_bit = on_bit==Bit_RESET?Bit_SET:Bit_RESET;
	this->is_on = false;
	this->pin->set_output(off_bit);
}


void Led::set_off(void)
{
	pin->write(off_bit);
	is_on = false;
}

void Led::set_on(void)
{
	pin->write(on_bit);
	is_on = true;
}

void Led::turn(void)
{
	if ( is_on ){
		set_off();
	}else{
		set_on();
	}
}
