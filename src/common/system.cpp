/*
 * system.cpp
 *
 *  Created on: Dec 21, 2018
 *      Author: zhan
 */

#include "system.h"

TIM_TypeDef *System::timx;
uint32_t System::system_ticks_us;
uint32_t System::system_ticks_ms;


void System::init(TIM_TypeDef *tim)
{
	// TIM2在APB1上，APB1预分频为1/4
	// TIM_CKD_DIV1 -> 1/2分频
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Period = 0xFFFFFFFF;
	TIM_TimeBaseInitStructure.TIM_Prescaler = 0;
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0x00;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);
	TIM_Cmd(tim, ENABLE);
	timx = tim;
	system_ticks_ms = SystemCoreClock/8/1000;
	system_ticks_us = system_ticks_ms/1000;
}


void System::msleep(uint32_t ms){
#if 0
	uint32_t t = get_tick();
	while ( get_tick() - t <  ms * system_ticks_ms);
#else
	while(ms--){
		usleep(1000);
	}
#endif
}


void System::usleep(uint32_t us){
	uint32_t t = get_tick();
	while ( get_tick() - t <  us * system_ticks_us);
}


uint32_t System::get_tick(void)
{
	return TIM_GetCounter(timx);
}


System::Timer::Timer()
{
	clear();
}

void System::Timer::clear()
{
	tick_stamp = get_tick();
}


bool System::Timer::is_exceed(uint32_t us)
{
	if ( get_tick() - tick_stamp >= us * system_ticks_us ){
		return true;
	}
	return false;
}


