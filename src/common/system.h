/*
 * system.h
 *
 *  Created on: Dec 21, 2018
 *      Author: zhan
 */

#ifndef COMMON_SYSTEM_H_
#define COMMON_SYSTEM_H_

#include "stm32f4xx.h"

class System{
private:
	static TIM_TypeDef *timx;
	static uint32_t system_ticks_us;
	static uint32_t system_ticks_ms;
public:
	static void init(TIM_TypeDef *tim);
	static void msleep(uint32_t ms);
	static void usleep(uint32_t us);
	static uint32_t get_tick();

	class Timer{
	private:
		uint32_t tick_stamp;
	public:
		Timer();
		void clear();
		bool is_exceed(uint32_t us);
	};
};



#endif /* COMMON_SYSTEM_H_ */
